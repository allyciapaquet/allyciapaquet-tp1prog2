class Formation{
    constructor(id, nom, description, cout){
        this.id = id;
        this.nom = nom;
        this.description = description; 
        this.cout = cout;
    }
}

const data = [   
    new Formation( 0, "Formation 1", "Pour apprendre a faire des gateaux", 100, "$"),
    new Formation( 1, "Formation 2", "Pour apprendre a conduire", 230, "$"),
    new Formation( 2, "Formation 3", "Pour apprendre a chanter de l'opera", 690, "$"),
    new Formation( 3, "Formation 4", "Pour apprendre a faire du velo les yeux fermes", 530, "$"),
    new Formation( 4, "Formation 5", "Pour apprendre a dormir", 460, "$"),
    new Formation( 5, "Formation 6", "Pour apprendre a conduire", 973, "$")
];

exports.getFormationById = function(id){
   return data[id]; 
}

exports.addFormation = function(dat){
    let id = data.length;
    data.push(new Formation(id, dat.nom, dat.description, dat.cout))
    return true;
}

exports.allFormation = data;
