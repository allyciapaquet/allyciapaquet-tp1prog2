var express = require('express');
var router = express.Router();
let formation = require("../custom_modules/formation.js")

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Libri', description: "Une plateforme pour afficher différents cours d'une école de formation continue", lorem: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Egestas maecenas pharetra convallis posuere. Pellentesque dignissim enim sit amet. Varius quam quisque id diam vel quam elementum pulvinar etiam. Ac tincidunt vitae semper quis lectus nulla at volutpat. Ullamcorper velit sed ullamcorper morbi tincidunt ornare massa eget. Nulla facilisi cras fermentum odio eu feugiat. Placerat orci nulla pellentesque dignissim enim. Congue mauris rhoncus aenean vel. Duis ut diam quam nulla porttitor massa id neque. Facilisis gravida neque convallis a cras semper auctor neque. Vitae congue eu consequat ac felis donec et odio. Morbi tristique senectus et netus et malesuada fames ac."});
});

router.get('/formation', function(req, res, next) {
  console.log(formation)
  res.render('formation', { title: 'Formation', description: "Voici les differentes formations offerte dans notre centre", texte: "la liste est ici", formation: formation.allFormation });
});

router.get('/formmongo', function(req, res, next) {
  res.render('formmongo', { title: 'Formation Mongo', description: "Voici la liste des formations completes sur MongoDB"});
});

router.get('/formnode', function(req, res, next) {
  res.render('formnode', { title: 'Formation Node', description: "Voici la liste des formations completes sur NodeJs"});
});

router.get('/contact', function(req, res, next) {
  res.render('contact', { title: 'Contact'});
});

router.get('/forma/add', function(req,res,next){
  res.render('add');
})



router.post('/forma/add', function(req,res,next){
  let cours = formation.addForma(req.body)
  if(cours){
    res.render('forma',{ title: 'La liste', description: "La liste absurde de l'ensemble de mes films", formation: formation.allFormation } )
  }
})




router.get('/blog', function(req, res, next) {
  res.render('blog', { article1: "Article 1", article2: "Article 2", article3: "Article 3", article4: "Article 4", article5: "Article 5", article6: "Article 6", article7: "Article 7", article8: "Article 8", article9: "Article 9", article10: "Article 10",});
});


// Pour un id precis
// router.get('/formation/:id', function(req, res, next){
//   res.render('formation', {formation: formation.getFormationById(req.params.id)})
// })

module.exports = router;